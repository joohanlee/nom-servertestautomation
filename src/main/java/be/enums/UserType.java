package be.enums;

public enum UserType {
    CUSTOMER, CREATOR, ADMIN
}
