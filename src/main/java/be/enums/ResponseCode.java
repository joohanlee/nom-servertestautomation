package be.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResponseCode {
    OK(20000),

    NOT_ABLE_DELETE_STORY_CATEGORY(40012),

    NOT_EXIST_TERM(40403),
    NOT_EXIST_TERM_CONTENT(40404),
    ACCOUNT_ALREADY_EXISTS_TERM(40922),
    ACCOUNT_ALREADY_EXISTS_TERM_CONTENT(40923),
    DUPLICATED_STORY_CATEGORY_NAME(40944);


    private int value;


}
