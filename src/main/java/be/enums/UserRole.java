package be.enums;

public enum UserRole {
    ROLE_CUSTOMER, ROLE_ADMIN, ROLE_CREATOR;
}
