package be.dto.domains.badge;

/**
 * Created by Joohan Lee on 2019-10-15
 */
public enum BadgeAwardType {
  AUTO, MANUAL
}
