package be.dto.domains.badge;

/**
 * Created by Joohan Lee on 2019-10-15
 */
public enum BadgeConditionCheck {
  MANUAL,
  GREATER_THAN,
  GREATER_THAN_OR_EQUAL,
  EQUAL,
  LESS_THAN_OR_EQUAL,
  LESS_THAN
}
