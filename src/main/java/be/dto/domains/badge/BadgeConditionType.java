package be.dto.domains.badge;

/**
 * Created by Joohan Lee on 2019-10-15
 */
public enum BadgeConditionType {
  MANUAL,
  LEVEL,
  GOODS,
  MESSAGE,
  CHEER_COMMENT,
  FAN,
  ARTWORK_ON_SALE,
  STICKER_ON_SALE,
  STOCK_IMAGE_ON_SALE,
  COLORING_SHEET_ON_SALE,
  AUDIO_ON_SALE,
  FEATURED_CREATOR;
}
