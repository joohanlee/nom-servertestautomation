package be.dto.domains;

import be.enums.UserRole;
import be.enums.UserStatus;
import be.enums.UserType;
import java.time.ZonedDateTime;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Getter
@Builder
public class MarketToken {
    private String transId;
    private String marketId;
    private String userId;
    private String username;
    private String nickName;
    private UserType userType;
    private String profileUrl;
    private UserStatus status;
    private UserRole role;
    private String marketUserId;
    private String extra;

    @Builder.Default
    private ZonedDateTime createdAt = ZonedDateTime.now();
}
