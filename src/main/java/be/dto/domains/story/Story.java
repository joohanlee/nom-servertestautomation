package be.dto.domains.story;

import java.util.List;
import lombok.Getter;

/**
 * Created by Joohan Lee on 2020/06/10
 */

@Getter
public class Story {
  private String title;
  private String content;
  private StoryStatus storyStatus;
  private String thumbnailImage;
  private String storyId;
  private StoryCategory category;
  private List<String> tags;
}
