package be.dto.domains.story;

/**
 * Created by Joohan Lee on 2020/06/10
 */
public enum StoryStatus {
  TEMPORARY, UPLOADED, IN_REVIEW, PUBLISHED
}
