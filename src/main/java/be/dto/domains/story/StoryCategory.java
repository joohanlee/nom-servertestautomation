package be.dto.domains.story;

import lombok.Getter;

/**
 * Created by Joohan Lee on 2020/06/09
 */

@Getter
public class StoryCategory {
  private String krName;
  private String enName;
  private int priority;
  private String categoryId;
}
