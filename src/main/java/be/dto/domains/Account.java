package be.dto.domains;

import be.common.HttpResponse;
import be.dto.domains.badge.BadgeConditionType;
import be.dto.domains.story.Story;
import be.dto.domains.story.StoryCategory;
import be.dto.request.badge.CreateBadgeRequestBody;
import be.dto.request.CreatePartnerContractRequestBody;
import be.dto.request.story.CreateStoryCategoryRequest;
import be.dto.request.story.CreateStoryAdminRequest;
import be.dto.request.story.UpdateStoryCategoryRequest;
import be.dto.request.story.UpdateStoryRequest;
import be.dto.request.term.CreateTermContentRequestBody;
import be.dto.request.term.CreateTermRequestBody;
import be.dto.request.term.UpdateTermContentRequestBody;
import be.dto.request.term.UpdateTermRequestBody;
import be.dto.response.badge.AdminBadgeResponse;
import be.dto.response.badge.CreatorBadgeResponse;
import be.dto.response.partner_contract.PartnerContractResponse;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Data;

@Data
public class Account {
    private String accessToken;
    private String userId;
    private HttpResponse lastResponse;

    private CreateTermRequestBody createdTermRequestBody;
    private UpdateTermRequestBody updatedTermRequestBody;
    private CreateTermContentRequestBody createdTermContentRequestBody;
    private UpdateTermContentRequestBody updatedTermContentRequestBody;

    private CreateBadgeRequestBody createBadgeRequestBody;
    private List<AdminBadgeResponse> adminBadgeListResponse;
    private Map<BadgeConditionType, List<CreatorBadgeResponse>> creatorBadgeListResponse;
    private String createdBadgeId;

    private List<PartnerContractResponse> partnerContracts;
    private PartnerContractResponse createdPartnerContract;
    private CreatePartnerContractRequestBody createPartnerContractRequestBody;


    private List<StoryCategory> storyCategories;
    private CreateStoryCategoryRequest createStoryCategoryRequest;
    private UpdateStoryCategoryRequest updateStoryCategoryRequest;
    private StoryCategory createdStoryCategory;
    private StoryCategory updatedStoryCategory;

    private List<Story> stories;
    private CreateStoryAdminRequest createStoryAdminRequest;
    private UpdateStoryRequest updateStoryRequest;
    private Story createdStory;
    private Story updatedStory;



    @Builder
    public Account(String accessToken, String userId) {
        this.accessToken = accessToken;
        this.userId = userId;
    }
}
