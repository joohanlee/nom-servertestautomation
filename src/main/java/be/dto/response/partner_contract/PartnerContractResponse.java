package be.dto.response.partner_contract;

import lombok.Data;
import lombok.Getter;

/**
 * Created by Joohan Lee on 2019/10/30
 */

@Data
public class PartnerContractResponse {
  private String creatorId;
  private String partnerContractId;
  private boolean massUploader;
}
