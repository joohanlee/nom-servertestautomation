package be.dto.response.badge;

import be.dto.domains.badge.BadgeConditionCheck;
import be.dto.domains.badge.BadgeConditionType;
import lombok.Data;

/**
 * Created by Joohan Lee on 16/10/2019
 */
@Data
public class BadgeConditionResponse {
  private String badgeConditionId;
  private BadgeConditionType badgeConditionType;
  private BadgeConditionCheck badgeConditionCheck;
  private String requiredValue;
}
