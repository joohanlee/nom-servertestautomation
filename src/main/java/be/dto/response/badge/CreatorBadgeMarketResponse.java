package be.dto.response.badge;

import lombok.Data;

/**
 * Created by Joohan Lee on 2019-10-15
 */
@Data
public class CreatorBadgeMarketResponse {
    private String name;
    private String description;
    private int priority;
    private String badgeFileKey;
    private String badgeFileUrl;
}
