package be.dto.response.badge;

import be.dto.domains.badge.BadgeAwardType;
import java.util.List;
import lombok.Data;

/**
 * Created by Joohan Lee on 16/10/2019
 */

@Data
public class CreatorBadgeResponse {
  private String badgeId;
  private String name;
  private String description;
  private String badgeFileKey;
  private String badgeFileUrl;
  private List<BadgeConditionResponse> badgeConditions;
  private BadgeAwardType badgeAwardType;
  private int priority;
  private boolean chosen;
  private boolean choiced;
}
