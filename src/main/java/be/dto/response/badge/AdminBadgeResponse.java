package be.dto.response.badge;

import be.dto.domains.badge.BadgeAwardType;
import java.util.List;
import lombok.Data;

/**
 * Created by Joohan Lee on 16/10/2019
 */
@Data
public class AdminBadgeResponse {
  private String badgeId;
  private String name;
  private String descriptionForCreator;
  private String descriptionForMarket;
  private String badgeFileKey;
  private String badgeFileUrl;
  private String disabledBadgeFileKey;
  private String disabledBadgeFileUrl;
  private List<BadgeConditionResponse> badgeConditions;
  private BadgeAwardType badgeAwardType;
  private boolean isDeleted;
  private int priority;
}
