package be.dto.request.badge;

import be.dto.domains.badge.BadgeConditionCheck;
import be.dto.domains.badge.BadgeConditionType;
import lombok.Builder;
import lombok.Getter;

/**
 * Created by Joohan Lee on 2019-10-15
 */
@Builder
@Getter
public class CreateBadgeConditionRequestBody {
  private BadgeConditionType badgeConditionType;
  private BadgeConditionCheck badgeConditionCheck;
  private String requiredValue;
}
