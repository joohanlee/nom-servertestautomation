package be.dto.request.badge;

import be.dto.domains.badge.BadgeAwardType;
import java.util.List;
import lombok.Builder;
import lombok.Getter;

/**
 * Created by Joohan Lee on 2019-10-15
 */
@Builder
@Getter
public class CreateBadgeRequestBody {
    private String name;
    private String descriptionForCreator;
    private String descriptionForMarket;
    private String badgeFileKey;
    private String disabledBadgeFileKey;
    private List<CreateBadgeConditionRequestBody> badgeConditions;
    private BadgeAwardType badgeAwardType;
    private boolean isDeleted;
    private int priority;
}
