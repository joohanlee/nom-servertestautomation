package be.dto.request;

import java.util.Set;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import lombok.ToString;

/**
 * Created by Joohan Lee on 2019/11/27
 */
@Builder
@Getter
public class UpdateArtworkCategoryRequestBody {
  @Singular
  private Set<String> categoryIds;
}
