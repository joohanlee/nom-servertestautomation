package be.dto.request;

import lombok.Builder;
import lombok.Getter;

/**
 * Created by Joohan Lee on 2019/10/30
 */
@Builder
@Getter
public class UpdatePartnerContractRequestBody {
  private String creatorId;
  private boolean massUploader;
}
