package be.dto.request.story;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Builder;
import lombok.Getter;

/**
 * Created by Joohan Lee on 2020/06/03
 */
@Getter
@JsonInclude(Include.NON_NULL)
@io.cucumber.datatable.dependency.com.fasterxml.jackson.annotation.JsonInclude
public class CreateStoryCategoryRequest {
  private int priority;
  private String krName;
  private String enName;

  @Builder
  public CreateStoryCategoryRequest(int priority, String krName, String enName) {
    this.priority = priority;
    this.krName = krName;
    this.enName = enName;
  }
}
