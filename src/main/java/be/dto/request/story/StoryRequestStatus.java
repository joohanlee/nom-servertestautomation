package be.dto.request.story;

/**
 * Created by Joohan Lee on 2020/06/03
 */

public enum StoryRequestStatus {
  TEMPORARY, UPLOADED
}
