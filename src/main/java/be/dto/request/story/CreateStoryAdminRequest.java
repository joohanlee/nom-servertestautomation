package be.dto.request.story;

import be.dto.domains.story.StoryStatus;
import java.util.List;
import lombok.Builder;
import lombok.Getter;

/**
 * Created by Joohan Lee on 2020/06/03
 */

@Builder
@Getter
public class CreateStoryAdminRequest {
  private String title;
  private String content;
  private List<String> tags;
  private String thumbnailImage;
  private String categoryId;
  private String creatorId;
  private StoryStatus storyStatus;
}