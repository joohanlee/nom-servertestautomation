package be.dto.request.story;

import lombok.Builder;
import lombok.Getter;

/**
 * Created by Joohan Lee on 2020/06/03
 */
@Getter
public class UpdateStoryCategoryRequest {
  private int priority;
  private String krName;
  private String enName;

  @Builder
  public UpdateStoryCategoryRequest(int priority, String krName, String enName) {
    this.priority = priority;
    this.krName = krName;
    this.enName = enName;
  }
}
