package be.dto.request.story;

import java.util.List;
import lombok.Builder;
import lombok.Getter;

/**
 * Created by Joohan Lee on 2020/06/05
 */

@Builder
@Getter
public class UpdateStoryRequest {
  private String title;
  private String content;
  private List<String> tags;
  private String thumbnailImage;
  private String categoryId;
}
