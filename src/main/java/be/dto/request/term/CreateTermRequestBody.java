package be.dto.request.term;

import be.enums.TermType;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Getter
@Builder
public class CreateTermRequestBody {
    private String name;
    private int displayOrder;
    private TermType termType;
    private boolean activated;
    private String termId;
}
