package be.dto.request.term;

import be.enums.TermType;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Getter
@Builder
public class UpdateTermRequestBody {
    private String name;
    private int displayOrder;
    private TermType termType;
    private boolean activated;
}
