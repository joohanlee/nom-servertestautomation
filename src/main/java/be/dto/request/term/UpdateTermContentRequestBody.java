package be.dto.request.term;

import be.enums.Language;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Getter
@Builder
public class UpdateTermContentRequestBody {
    private String title;
    private String content;
    private Language language;
}
