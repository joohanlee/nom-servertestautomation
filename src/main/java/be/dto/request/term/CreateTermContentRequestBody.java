package be.dto.request.term;

import be.enums.Language;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Getter
@Builder
public class CreateTermContentRequestBody {
    private String title;
    private String content;
    private Language language;
}
