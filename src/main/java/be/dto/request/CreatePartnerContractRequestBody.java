package be.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

/**
 * Created by Joohan Lee on 2019/10/30
 */

@Builder
@Getter
public class CreatePartnerContractRequestBody {
  private String creatorId;
  private boolean massUploader;
}
