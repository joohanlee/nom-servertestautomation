package be.common;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.hamcrest.Matcher;

/**
 * Created by Joohan Lee on 17/10/2019
 */
public class HttpResponse {
  private Response response;
  private static ObjectMapper objectMapper = new ObjectMapper();

  public HttpResponse(Response response) {
    this.response = response;
  }

  public int getInt(String jsonPath) {
    return jsonPath().getInt(jsonPath);
  }

  public String getString(String jsonPath) {
    return jsonPath().getString("data.badgeId");
  }

  public <T> T getObject(String jsonPath, Class<T> tClass) {
    return objectMapper.convertValue(jsonPath().get(jsonPath), tClass);
  }

  public <T> T getObject(String jsonPath, TypeReference<T> typeReference) {
    return objectMapper.convertValue(jsonPath().get(jsonPath), typeReference);
  }

  public HttpResponse statusMatches(HttpStatus statusCode) {
    response.then().statusCode(statusCode.getCode());

    return this;
  }

  public HttpResponse bodyMatches(String jsonPath, Matcher matcher) {
    response.then().body(jsonPath, matcher);

    return this;
  }

  private JsonPath jsonPath() {
    return response.body()
        .jsonPath();
  }
}
