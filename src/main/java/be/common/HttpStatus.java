package be.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by Joohan Lee on 2020/06/09
 */


@Getter
@AllArgsConstructor
public enum HttpStatus {
  OK(200),
  BAD_REQUEST(400),
  CONFLICT(409);

  private int code;
}
