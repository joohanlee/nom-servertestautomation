package be.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OGQMarketURL {

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Account {

        @NoArgsConstructor(access = AccessLevel.PRIVATE)
        public static class Admin {
            public static final String USER_TERMS = "account/admin/terms";

            public static final String USER_TERM = "account/admin/terms/{termId}";
            public static final String USER_TERM_CONTENT = "account/admin/terms/{termId}/contents";

            public static final String GET_PARTNER_CONTRACTS = "account/admin/creators/partnerContracts";
            public static final String CREATE_PARTNER_CONTRACTS = "account/admin/creators/partnerContracts";
            public static final String UPDATE_PARTNER_CONTRACTS = "account/admin/creators/partnerContracts/{partnerContractId}";
            public static final String DELETE_PARTNER_CONTRACTS = "account/admin/creators/partnerContracts/{partnerContractId}";
        }
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Content {

        @NoArgsConstructor(access = AccessLevel.PRIVATE)
        public static class Admin {
            public static final String BADGE = "content/admin/badge";
            public static final String DELETE_BADGE = "content/admin/badge/{badgeId}";
            public static final String CHANGE_CATEGORY = "content/admin/artworks/{artworkId}/categories";

            public static final String GET_STORY_CATEGORY_LIST = "content/admin/storyCategory";
            public static final String CREATE_STORY_CATEGORY = "content/admin/storyCategory";
            public static final String UPDATE_STORY_CATEGORY = "content/admin/storyCategory/{categoryId}";
            public static final String DELETE_STORY_CATEGORY = "content/admin/storyCategory/{categoryId}";

            public static final String GET_STORY_LIST = "content/admin/story";
            public static final String GET_STORY_DETAIL = "content/admin/story/{storyId}";
            public static final String CREATE_STORY = "content/admin/story";
            public static final String UPDATE_STORY = "content/admin/story/{storyId}";
            public static final String DELETE_STORY = "content/admin/story/{storyId}";
        }

        @NoArgsConstructor(access = AccessLevel.PRIVATE)
        public static class Creator {
            public static final String GET_BADGES = "content/creator/badges";
            public static final String GET_BADGE_DETAIL = "content/creator/badges/{badgeId}";

        }

        @NoArgsConstructor(access = AccessLevel.PRIVATE)
        public static class Customer {
            public static final String BADGES_OF_CREATOR = "content/market/badges/creator/{creatorId}";
        }
    }


}
