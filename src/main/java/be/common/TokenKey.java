package be.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Created by Joohan Lee on 2020/01/14
 */

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TokenKey {
  public static final String MARKET_NAME = "NOM";
  public static final String MARKET_ID = "mId";
  public static final String USER_ID = "uId";
  public static final String USER_NAME = "name";
  public static final String NICK_NAME = "nick";
  public static final String USER_TYPE = "type";
  public static final String USER_IMAGE = "image";
  public static final String USER_STATUS = "status";
  public static final String USER_ROLE = "role";
  public static final String MARKET_USER_ID = "muId";
  public static final String EXTRA = "extra";
}
