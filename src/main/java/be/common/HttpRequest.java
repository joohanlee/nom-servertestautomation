package be.common;

import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import java.util.Map;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;
import net.serenitybdd.rest.SerenityRest;

@Data
@Builder
public class HttpRequest {
    @Builder.Default
    private ContentType contentType = ContentType.JSON;

    @Singular
    private Map<String, Object> pathParams;

    @Singular
    private Map<String, Object> queryParams;

    private String requestURL;
    private String accessToken;
    private Object requestBody;

    public HttpResponse get() {
        return new HttpResponse(
            createRequest()
                .get(requestURL)
                .andReturn()
        );
    }


    public HttpResponse post() {
        return new HttpResponse(
            createRequest()
                .body(requestBody)
                .post(requestURL)
                .andReturn()
        );
    }

    public HttpResponse put() {
        return new HttpResponse(
            createRequest()
                .body(requestBody)
                .put(requestURL)
                .andReturn()
        );
    }

    public HttpResponse delete() {
        return new HttpResponse(
            createRequest()
                .delete(requestURL)
                .andReturn()
        );
    }

    private RequestSpecification createRequest() {
        return SerenityRest.given()
            .contentType(contentType)
            .header("X-MARKET-AUTH-TOKEN", accessToken)
            .pathParams(pathParams)
            .queryParams(queryParams);
    }
}
