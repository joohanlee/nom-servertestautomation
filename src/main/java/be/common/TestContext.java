package be.common;


import be.dto.domains.Account;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class TestContext {

    private static EnvironmentVariables environmentVariables = SystemEnvironmentVariables.createEnvironmentVariables();

    public static Account getAccount(String accountKey) {
        return Serenity.sessionVariableCalled(accountKey);
    }

    public static void setAccount(String accountKey, Account account) {
        Serenity.setSessionVariable(accountKey).to(account);
    }

    public static String getSystemVariable(String key) {
        return environmentVariables.getProperty(key);
    }

    public static int getSystemVariableAsInt(String key) {
        return Integer.parseInt(environmentVariables.getProperty(key));
    }
}