package be.common;

import static be.common.TokenKey.EXTRA;
import static be.common.TokenKey.MARKET_ID;
import static be.common.TokenKey.MARKET_NAME;
import static be.common.TokenKey.MARKET_USER_ID;
import static be.common.TokenKey.NICK_NAME;
import static be.common.TokenKey.USER_ID;
import static be.common.TokenKey.USER_IMAGE;
import static be.common.TokenKey.USER_NAME;
import static be.common.TokenKey.USER_ROLE;
import static be.common.TokenKey.USER_STATUS;
import static be.common.TokenKey.USER_TYPE;
import static be.enums.UserRole.ROLE_ADMIN;
import static be.enums.UserRole.ROLE_CREATOR;
import static be.enums.UserStatus.ENABLE;

import be.dto.domains.MarketToken;
import be.enums.UserRole;
import be.enums.UserStatus;
import be.enums.UserType;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.util.Date;
import org.junit.Test;


public class MarketTokenManager {
    private static String SECRET_KEY = "wFJM4HvazqPPRHHTaxolA47CHZmG0gJO";

    public static String generate(MarketToken marketToken) {
        Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY);
        return JWT.create()
            .withIssuer(MARKET_NAME)
            .withIssuedAt(new Date(marketToken.getCreatedAt().toEpochSecond() * 1000))
            .withClaim(MARKET_ID, marketToken.getMarketId())
            .withClaim(USER_ID, marketToken.getUserId())
            .withClaim(USER_NAME, marketToken.getUsername())
            .withClaim(NICK_NAME, marketToken.getNickName())
            .withClaim(USER_TYPE, marketToken.getUserType().name())
            .withClaim(USER_IMAGE, marketToken.getProfileUrl())
            .withClaim(USER_STATUS, marketToken.getStatus().name())
            .withClaim(USER_ROLE, marketToken.getRole().name())
            .withClaim(MARKET_USER_ID, marketToken.getMarketUserId())
            .withClaim(EXTRA, marketToken.getExtra())
            .sign(algorithm);
    }

    public static MarketToken decode(String accessToken) {
        DecodedJWT decodedJWT = JWT.decode(accessToken);

        System.out.println(decodedJWT.getHeader());
        System.out.println(decodedJWT.getPayload());
        System.out.println(decodedJWT.getSignature());
        System.out.println(decodedJWT.getToken());

        return MarketToken.builder()
            .marketId(decodedJWT.getClaim(MARKET_ID).asString())
            .userId(decodedJWT.getClaim(USER_ID).asString())
            .username(decodedJWT.getClaim(USER_NAME).asString())
            .nickName(decodedJWT.getClaim(NICK_NAME).asString())
            .userType(decodedJWT.getClaim(USER_TYPE).as(UserType.class))
            .profileUrl(decodedJWT.getClaim(USER_IMAGE).asString())
            .status(decodedJWT.getClaim(USER_STATUS).as(UserStatus.class))
            .role(decodedJWT.getClaim(USER_ROLE).as(UserRole.class))
            .marketUserId(decodedJWT.getClaim(MARKET_USER_ID).asString())
            .extra(decodedJWT.getClaim(EXTRA).asString())
            .build();
    }
}
