package be.steps;

import be.common.HttpRequest;
import be.common.OGQMarketURL.Content.Admin;
import be.dto.domains.Account;
import be.dto.request.UpdateArtworkCategoryRequestBody;

/**
 * Created by Joohan Lee on 2019/11/27
 */

public class AdminArtworkSteps {
  public void changeCategory(Account account, String artworkId, String categoryId) {
    UpdateArtworkCategoryRequestBody updateArtworkCategoryRequestBody = UpdateArtworkCategoryRequestBody.builder()
        .categoryId(categoryId)
        .build();

    HttpRequest request = HttpRequest.builder()
        .accessToken(account.getAccessToken())
        .requestURL(Admin.CHANGE_CATEGORY)
        .pathParam("artworkId", artworkId)
        .requestBody(updateArtworkCategoryRequestBody)
        .build();

    account.setLastResponse(request.put());
  }
}
