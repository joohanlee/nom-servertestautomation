package be.steps;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

import be.common.HttpRequest;
import be.common.OGQMarketURL;
import be.dto.domains.Account;
import be.dto.request.term.CreateTermContentRequestBody;
import be.dto.request.term.CreateTermRequestBody;
import be.dto.request.term.UpdateTermContentRequestBody;
import be.dto.request.term.UpdateTermRequestBody;
import be.enums.Language;
import be.enums.ResponseCode;
import be.enums.TermType;
import net.thucydides.core.annotations.Step;
import org.hamcrest.core.Every;

public class AdminTermSteps {
    @Step
    public void getTerm(Account account, String termId) {
        HttpRequest request = HttpRequest.builder()
            .accessToken(account.getAccessToken())
            .requestURL(OGQMarketURL.Account.Admin.USER_TERM)
            .pathParam("termId", termId)
            .build();

        account.setLastResponse(request.get());
    }

    @Step
    public void getTermContent(Account account, String termId) {
        HttpRequest request = HttpRequest.builder()
            .accessToken(account.getAccessToken())
            .requestURL(OGQMarketURL.Account.Admin.USER_TERM_CONTENT)
            .pathParam("termId", termId)
            .build();

        account.setLastResponse(request.get());
    }

    @Step("Search on user terms")
    public void searchOnTerm(Account account, String name, String activated, TermType termType, int page, int pageSize) {
        HttpRequest request = HttpRequest.builder()
            .accessToken(account.getAccessToken())
            .requestURL(OGQMarketURL.Account.Admin.USER_TERMS)
            .queryParam("name", name)
            .queryParam("activated", activated)
            .queryParam("termType", termType)
            .queryParam("page", page)
            .queryParam("pageSize", pageSize)
            .build();

        account.setLastResponse(request.get());
    }

    @Step
    public void createTerm(Account account, String termId, String name, int displayOrder, TermType termType, boolean activated) {
        CreateTermRequestBody createTermRequestBody = CreateTermRequestBody.builder()
            .name(name)
            .activated(activated)
            .displayOrder(displayOrder)
            .termType(termType)
            .termId(termId)
            .build();

        HttpRequest request = HttpRequest.builder()
            .accessToken(account.getAccessToken())
            .requestURL(OGQMarketURL.Account.Admin.USER_TERMS)
            .requestBody(createTermRequestBody)
            .build();

        account.setLastResponse(request.post());
        account.setCreatedTermRequestBody(createTermRequestBody);
    }

    @Step
    public void updateTerm(Account account, String termId, String name, int displayOrder, TermType termType, boolean activated) {
        UpdateTermRequestBody updateTermRequestBody = UpdateTermRequestBody.builder()
            .name(name)
            .activated(activated)
            .displayOrder(displayOrder)
            .termType(termType)
            .build();

        HttpRequest request = HttpRequest.builder()
            .accessToken(account.getAccessToken())
            .requestURL(OGQMarketURL.Account.Admin.USER_TERM)
            .pathParam("termId", termId)
            .requestBody(updateTermRequestBody)
            .build();

        account.setLastResponse(request.put());
        account.setUpdatedTermRequestBody(updateTermRequestBody);

    }

    @Step
    public void createTermContent(Account account, String termId, String title, String content, Language language) {
        CreateTermContentRequestBody createTermContentRequestBody = CreateTermContentRequestBody.builder()
            .title(title)
            .content(content)
            .language(language)
            .build();

        HttpRequest request = HttpRequest.builder()
            .accessToken(account.getAccessToken())
            .requestURL(OGQMarketURL.Account.Admin.USER_TERM_CONTENT)
            .pathParam("termId", termId)
            .requestBody(createTermContentRequestBody)
            .build();

        account.setLastResponse(request.post());
        account.setCreatedTermContentRequestBody(createTermContentRequestBody);
    }

    @Step
    public void updateTermContent(Account account, String termId, String title, String content, Language language) {
        UpdateTermContentRequestBody updateTermContentRequestBody = UpdateTermContentRequestBody.builder()
            .title(title)
            .content(content)
            .language(language)
            .build();

        HttpRequest request = HttpRequest.builder()
            .accessToken(account.getAccessToken())
            .requestURL(OGQMarketURL.Account.Admin.USER_TERM_CONTENT)
            .pathParam("termId", termId)
            .requestBody(updateTermContentRequestBody)
            .build();

        account.setLastResponse(request.put());
        account.setUpdatedTermContentRequestBody(updateTermContentRequestBody);
    }

    @Step
    public void validateSearchTermResult(Account account, String activated, TermType termType, int limit) {
        account.getLastResponse()
            .bodyMatches("code", is(ResponseCode.OK.getValue()))
            .bodyMatches("data.terms.termType", Every.everyItem(is(termType.toString())))
            .bodyMatches("data.terms.activated", Every.everyItem(is(Boolean.valueOf(activated))))
            .bodyMatches("data.terms", hasSize(lessThanOrEqualTo(limit)));
    }

    @Step
    public void validateCreateTermByGetTermResponse(Account account) {
        final String defaultCreatedVersion = "1";
        CreateTermRequestBody expectedTerm = account.getCreatedTermRequestBody();

        account.getLastResponse()
            .bodyMatches("code", is(ResponseCode.OK.getValue()))
            .bodyMatches("data.name", is(expectedTerm.getName()))
            .bodyMatches("data.activated", is(expectedTerm.isActivated()))
            .bodyMatches("data.currentVer", is(defaultCreatedVersion))
            .bodyMatches("data.displayOrder", is(expectedTerm.getDisplayOrder()))
            .bodyMatches("data.termType", is(expectedTerm.getTermType().toString()))
            .bodyMatches("data.termId", is(account.getCreatedTermRequestBody().getTermId()));
    }

    @Step
    public void validateUpdateTermByGetTermResponse(Account account) {
        UpdateTermRequestBody expectedTerm = account.getUpdatedTermRequestBody();

        account.getLastResponse()
            .bodyMatches("code", is(ResponseCode.OK.getValue()))
            .bodyMatches("data.name", is(expectedTerm.getName()))
            .bodyMatches("data.activated", is(expectedTerm.isActivated()))
            .bodyMatches("data.displayOrder", is(expectedTerm.getDisplayOrder()))
            .bodyMatches("data.termType", is(expectedTerm.getTermType().toString()));
    }

    @Step
    public void validateCreatedTermContent(Account account) {
        CreateTermContentRequestBody expectedTermContent = account.getCreatedTermContentRequestBody();

        account.getLastResponse()
            .bodyMatches("code", is(ResponseCode.OK.getValue()))
            .bodyMatches("data.termContents.title", containsInAnyOrder(expectedTermContent.getTitle()))
            .bodyMatches("data.termContents.content", containsInAnyOrder(expectedTermContent.getContent()))
            .bodyMatches("data.termContents.language", containsInAnyOrder(expectedTermContent.getLanguage().toString()));
    }

    @Step
    public void validateUpdateTermContentByGetTermContentResponse(Account account) {
        UpdateTermContentRequestBody expectedTermContent = account.getUpdatedTermContentRequestBody();

        account.getLastResponse()
            .bodyMatches("code", is(ResponseCode.OK.getValue()))
            .bodyMatches("data.termContents.title", containsInAnyOrder(expectedTermContent.getTitle()))
            .bodyMatches("data.termContents.content", containsInAnyOrder(expectedTermContent.getContent()))
            .bodyMatches("data.termContents.language", containsInAnyOrder(expectedTermContent.getLanguage().toString()));

    }
}
