package be.steps;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.text.MatchesPattern.matchesPattern;

import be.common.HttpRequest;
import be.common.HttpResponse;
import be.common.OGQMarketURL.Content.Admin;
import be.dto.domains.Account;
import be.dto.request.badge.CreateBadgeConditionRequestBody;
import be.dto.request.badge.CreateBadgeRequestBody;
import be.dto.response.badge.AdminBadgeResponse;
import be.enums.ResponseCode;
import com.fasterxml.jackson.core.type.TypeReference;
import java.util.List;

/**
 * Created by Joohan Lee on 2019-10-15
 */
public class AdminBadgeSteps {
  public void createBadge(Account account, CreateBadgeRequestBody createBadgeRequestBody) {
    HttpRequest request = HttpRequest.builder()
        .requestURL(Admin.BADGE)
        .accessToken(account.getAccessToken())
        .requestBody(createBadgeRequestBody)
        .build();

    account.setCreateBadgeRequestBody(createBadgeRequestBody);

    HttpResponse response = request.post();
    String createdBadgeId = response.getString("data.badgeId");

    account.setLastResponse(response);
    account.setCreatedBadgeId(createdBadgeId);
  }

  public void getBadges(Account account) {
    HttpRequest request = HttpRequest.builder()
        .requestURL(Admin.BADGE)
        .accessToken(account.getAccessToken())
        .build();

    HttpResponse response = request.get();
    List<AdminBadgeResponse> adminBadgeResponses = response.getObject("data.badges", new TypeReference<List<AdminBadgeResponse>>() {});

    account.setAdminBadgeListResponse(adminBadgeResponses);
    account.setLastResponse(response);
  }

  public void validateBadgeCreatedResponse(Account account) {
    CreateBadgeRequestBody createBadgeRequestBody = account.getCreateBadgeRequestBody();
    List<CreateBadgeConditionRequestBody> createBadgeConditionRequestBody = createBadgeRequestBody.getBadgeConditions();

    account.getLastResponse()
        .bodyMatches("code", is(ResponseCode.OK.getValue()))
        .bodyMatches("data.badgeId", notNullValue())
        .bodyMatches("data.name", is(createBadgeRequestBody.getName()))
        .bodyMatches("data.descriptionForCreator", is(createBadgeRequestBody.getDescriptionForCreator()))
        .bodyMatches("data.descriptionForMarket", is(createBadgeRequestBody.getDescriptionForMarket()))
        .bodyMatches("data.badgeFileKey", is(createBadgeRequestBody.getBadgeFileKey()))
        .bodyMatches("data.badgeFileUrl", matchesPattern("http://.*" + createBadgeRequestBody.getBadgeFileKey()))
        .bodyMatches("data.disabledBadgeFileKey", is(createBadgeRequestBody.getDisabledBadgeFileKey()))
        .bodyMatches("data.disabledBadgeFileUrl", matchesPattern("http://.*" + createBadgeRequestBody.getDisabledBadgeFileKey()))
        .bodyMatches("data.badgeAwardType", is(createBadgeRequestBody.getBadgeAwardType().toString()))
        .bodyMatches("data.priority", is(createBadgeRequestBody.getPriority()))
        .bodyMatches("data.deleted", is(false));

    for(int i = 0; i < createBadgeConditionRequestBody.size(); i++) {
      account.getLastResponse()
          .bodyMatches(String.format("data.badgeConditions[%d].badgeConditionId", i), notNullValue())
          .bodyMatches(String.format("data.badgeConditions[%d].badgeConditionType", i), is(createBadgeRequestBody.getBadgeConditions().get(i).getBadgeConditionType().toString()))
          .bodyMatches(String.format("data.badgeConditions[%d].badgeConditionCheck", i), is(createBadgeRequestBody.getBadgeConditions().get(i).getBadgeConditionCheck().toString()))
          .bodyMatches(String.format("data.badgeConditions[%d].requiredValue", i), is(createBadgeRequestBody.getBadgeConditions().get(i).getRequiredValue()));
    }
  }

  public void deleteBadge(Account account, String badgeId) {
    HttpRequest request = HttpRequest.builder()
        .requestURL(Admin.DELETE_BADGE)
        .pathParam("badgeId", badgeId)
        .accessToken(account.getAccessToken())
        .build();

    account.setLastResponse(request.delete());
  }

  public void validateBadgeInTheList(Account account, String badgeId) {
    assertThat(String.format("The badge[ID: %s] is not in the admin badges list response", badgeId), containInBadgeList(account, badgeId));
  }

  public void validateBadgeNotInTheList(Account account, String badgeId) {
    assertThat("The badge[ID: %s] is not in the admin badges list response", !containInBadgeList(account, badgeId));
  }

  private boolean containInBadgeList(Account account, String badgeId) {
    return account.getAdminBadgeListResponse()
        .stream()
        .map(AdminBadgeResponse::getBadgeId)
        .anyMatch(badgeId::equals);
  }
}
