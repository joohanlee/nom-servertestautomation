package be.steps;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.core.IsNot.not;

import be.common.HttpRequest;
import be.common.HttpResponse;
import be.common.OGQMarketURL.Account.Admin;
import be.dto.domains.Account;
import be.dto.request.CreatePartnerContractRequestBody;
import be.dto.request.UpdatePartnerContractRequestBody;
import be.dto.response.partner_contract.PartnerContractResponse;
import be.enums.ResponseCode;
import com.fasterxml.jackson.core.type.TypeReference;
import java.util.List;
import net.thucydides.core.annotations.Step;

/**
 * Created by Joohan Lee on 2019/10/30
 */
public class PartnerContractSteps {
  @Step
  public void createPartnerContract(Account admin, String creatorId, boolean isMassUploader) {
    CreatePartnerContractRequestBody createPartnerContractRequestBody = CreatePartnerContractRequestBody.builder()
        .creatorId(creatorId)
        .massUploader(isMassUploader)
        .build();

    HttpRequest request = HttpRequest.builder()
        .accessToken(admin.getAccessToken())
        .requestURL(Admin.CREATE_PARTNER_CONTRACTS)
        .requestBody(createPartnerContractRequestBody)
        .build();

    HttpResponse response = request.post();

    admin.setCreatePartnerContractRequestBody(createPartnerContractRequestBody);
    admin.setCreatedPartnerContract(response.getObject("data", PartnerContractResponse.class));
    admin.setLastResponse(response);
  }

  @Step
  public void getPartnerContracts(Account account) {
    HttpRequest request = HttpRequest.builder()
        .accessToken(account.getAccessToken())
        .requestURL(Admin.GET_PARTNER_CONTRACTS)
        .build();

    HttpResponse response = request.get();

    List<PartnerContractResponse> partnerContracts = response
        .getObject("data.partnerContracts", new TypeReference<List<PartnerContractResponse>>() {});

    account.setPartnerContracts(partnerContracts);
    account.setLastResponse(response);
  }

  @Step
  public void updatePartnerContract(Account account, String creatorId, boolean isMassUploader) {
    String partnerContractId = account.getPartnerContracts().get(0).getPartnerContractId();
    UpdatePartnerContractRequestBody updatePartnerContractRequestBody = UpdatePartnerContractRequestBody.builder()
        .creatorId(creatorId)
        .massUploader(isMassUploader)
        .build();

    HttpResponse response = HttpRequest.builder()
        .accessToken(account.getAccessToken())
        .requestURL(Admin.UPDATE_PARTNER_CONTRACTS)
        .pathParam("partnerContractId", partnerContractId)
        .requestBody(updatePartnerContractRequestBody)
        .build()
        .put();

    account.setLastResponse(response);
  }


  @Step
  public void deleteAllPartnerContracts(Account account) {
    account.getPartnerContracts().stream()
        .map(PartnerContractResponse::getPartnerContractId)
        .forEach(creatorId ->
            HttpRequest.builder()
                .accessToken(account.getAccessToken())
                .requestURL(Admin.DELETE_PARTNER_CONTRACTS)
                .pathParam("partnerContractId", creatorId)
                .build()
                .delete());
  }

  @Step
  public void validatePartnerContractCreated(Account account) {
    CreatePartnerContractRequestBody createPartnerContractRequestBody = account.getCreatePartnerContractRequestBody();
    PartnerContractResponse partnerContractResponse = account.getCreatedPartnerContract();
    account.getLastResponse()
        .bodyMatches("code", is(ResponseCode.OK.getValue()));

    assertThat(partnerContractResponse.getCreatorId(), is(createPartnerContractRequestBody.getCreatorId()));
    assertThat(partnerContractResponse.isMassUploader(), is(createPartnerContractRequestBody.isMassUploader()));
    assertThat(partnerContractResponse.getPartnerContractId(), not(isEmptyOrNullString()));
  }

  @Step
  public void validateCreatedPartnerContractInList(Account account) {
    PartnerContractResponse partnerContractResponse = account.getCreatedPartnerContract();

    assertThat(account.getPartnerContracts(), contains(partnerContractResponse));
  }

  @Step
  public void validatePartnerContractEmpty(Account account) {
    account.getLastResponse()
        .bodyMatches("code", is(ResponseCode.OK.getValue()));

    assertThat(account.getPartnerContracts(), empty());
  }

  @Step
  public void validateFirstPartnerContract(Account account, String creatorId, boolean massUploader) {
    PartnerContractResponse partnerContract = account.getPartnerContracts().get(0);

    assertThat(partnerContract.isMassUploader(), is(massUploader));
    assertThat(partnerContract.getCreatorId(), is(creatorId));
  }
}
