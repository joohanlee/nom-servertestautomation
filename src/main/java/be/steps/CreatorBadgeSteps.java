package be.steps;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

import be.common.HttpRequest;
import be.common.HttpResponse;
import be.common.OGQMarketURL.Content.Creator;
import be.dto.domains.Account;
import be.dto.domains.badge.BadgeConditionType;
import be.dto.response.badge.CreatorBadgeResponse;
import be.enums.ResponseCode;
import com.fasterxml.jackson.core.type.TypeReference;
import java.util.List;
import java.util.Map;
import net.thucydides.core.annotations.Step;

/**
 * Created by Joohan Lee on 2019-10-15
 */
public class CreatorBadgeSteps {
  @Step("Get creator's badge list")
  public void getCreatorBadges(Account account) {
    HttpRequest request = HttpRequest.builder()
        .accessToken(account.getAccessToken())
        .requestURL(Creator.GET_BADGES)
        .build();

    HttpResponse response = request.get();
    Map<BadgeConditionType, List<CreatorBadgeResponse>> creatorBadgeResponses = response
        .getObject("data.creatorsBadges", new TypeReference<Map<BadgeConditionType, List<CreatorBadgeResponse>>>() {});

    account.setCreatorBadgeListResponse(creatorBadgeResponses);
    account.setLastResponse(response);
  }

  @Step("Get creator's badge detail")
  public void getBadgeDetail(Account account, String creatorId) {
    HttpRequest request = HttpRequest.builder()
        .accessToken(account.getAccessToken())
        .requestURL(Creator.GET_BADGE_DETAIL)
        .pathParam("badgeId", creatorId)
        .build();

    account.setLastResponse(request.get());
  }

  @Step
  public void validateBadgeListResponse(Account account) {
    HttpResponse response = account.getLastResponse();
    Map<BadgeConditionType, List<CreatorBadgeResponse>> creatorBadgeResponses = account.getCreatorBadgeListResponse();

    assertThat(response.getInt("code"), is(ResponseCode.OK.getValue()));
    assertThat("The creator does not have any badge", not(creatorBadgeResponses.isEmpty()));
  }
}
