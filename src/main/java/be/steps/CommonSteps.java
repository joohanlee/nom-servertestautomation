package be.steps;

import static org.hamcrest.Matchers.is;

import be.common.HttpResponse;
import be.common.HttpStatus;
import be.enums.ResponseCode;
import net.thucydides.core.annotations.Step;

public class CommonSteps {
    @Step
    public void validateResponseCode(HttpResponse response, HttpStatus httpStatus, ResponseCode responseCode) {
        response.statusMatches(httpStatus)
            .bodyMatches("code", is(responseCode.getValue()));
    }

    @Step
    public void validateResponseStatus(HttpResponse response, HttpStatus httpStatus) {
        response.statusMatches(httpStatus);
    }
}
