package be.steps;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

import be.common.HttpRequest;
import be.common.OGQMarketURL.Content.Customer;
import be.dto.domains.Account;
import be.dto.response.badge.CreatorBadgeMarketResponse;
import be.enums.ResponseCode;
import com.fasterxml.jackson.core.type.TypeReference;
import java.util.List;
import net.thucydides.core.annotations.Step;
import org.hamcrest.collection.IsEmptyCollection;

/**
 * Created by Joohan Lee on 2019-10-15
 */
public class CustomerBadgeSteps {

  @Step("Get creator's badge in customer site")
  public void getCreatorBadges(Account account, String creatorId) {
    HttpRequest request = HttpRequest.builder()
        .accessToken(account.getAccessToken())
        .requestURL(Customer.BADGES_OF_CREATOR)
        .pathParam("creatorId", creatorId)
        .build();

    account.setLastResponse(request.get());
  }

  @Step("Validate creator badges response in customer site")
  public void validateCreatorBadges(Account account) {
    List<CreatorBadgeMarketResponse> creatorBadgeMarketResponses = account.getLastResponse()
        .getObject("data.creatorBadgeMarketResponses", new TypeReference<List<CreatorBadgeMarketResponse>>() {});

    int statusCode = account.getLastResponse()
        .getInt("code");

    assertThat(creatorBadgeMarketResponses, not(IsEmptyCollection.empty()));
    assertThat(statusCode, is(ResponseCode.OK.getValue()));
  }

}
