package be.steps;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.text.IsEmptyString.isEmptyString;

import be.common.HttpRequest;
import be.common.HttpResponse;
import be.common.HttpStatus;
import be.common.OGQMarketURL.Content.Admin;
import be.dto.domains.Account;
import be.dto.domains.story.StoryCategory;
import be.dto.request.story.CreateStoryCategoryRequest;
import be.dto.request.story.UpdateStoryCategoryRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import java.util.List;
import net.thucydides.core.annotations.Step;

/**
 * Created by Joohan Lee on 2020/06/08
 */
public class AdminStoryCategorySteps {
  @Step
  public void create(Account account, String krName, String enName, int priority) {
    CreateStoryCategoryRequest createStoryCategoryRequest = CreateStoryCategoryRequest.builder()
        .krName(krName)
        .enName(enName)
        .priority(priority)
        .build();

    HttpRequest request = HttpRequest.builder()
        .accessToken(account.getAccessToken())
        .requestURL(Admin.CREATE_STORY_CATEGORY)
        .requestBody(createStoryCategoryRequest)
        .build();

    HttpResponse response = request.post();

    account.setCreateStoryCategoryRequest(createStoryCategoryRequest);
    account.setLastResponse(response);
  }

  @Step
  public void updateCreatedStoryCategory(Account account, String krName, String enName, int priority) {
    UpdateStoryCategoryRequest updateStoryCategoryRequest = UpdateStoryCategoryRequest.builder()
        .krName(krName)
        .enName(enName)
        .priority(priority)
        .build();

    HttpRequest request = HttpRequest.builder()
        .accessToken(account.getAccessToken())
        .requestURL(Admin.UPDATE_STORY_CATEGORY)
        .pathParam("categoryId", account.getCreatedStoryCategory().getCategoryId())
        .requestBody(updateStoryCategoryRequest)
        .build();

    HttpResponse httpResponse = request.put();

    account.setUpdateStoryCategoryRequest(updateStoryCategoryRequest);
    account.setUpdatedStoryCategory(httpResponse.getObject("data", StoryCategory.class));
    account.setLastResponse(httpResponse);
  }

  @Step
  public void getList(Account account) {
    HttpRequest request = HttpRequest.builder()
        .accessToken(account.getAccessToken())
        .requestURL(Admin.GET_STORY_CATEGORY_LIST)
        .build();

    HttpResponse httpResponse = request.get();

    account.setLastResponse(httpResponse);
    account.setStoryCategories(
        httpResponse.getObject("data", new TypeReference<List<StoryCategory>>() {})
    );
  }


  @Step
  public void deleteEnNameStartingWith(Account account, String enName) {
    account.getStoryCategories().stream()
        .filter(storyCategory -> storyCategory.getEnName().startsWith(enName))
        .map(StoryCategory::getCategoryId)
        .forEach(categoryId -> delete(account, categoryId).statusMatches(HttpStatus.OK));
  }

  public HttpResponse delete(Account account, String categoryId) {
    HttpRequest request = HttpRequest.builder()
        .accessToken(account.getAccessToken())
        .requestURL(Admin.DELETE_STORY_CATEGORY)
        .pathParam("categoryId", categoryId)
        .build();

    return request.delete();
  }

  @Step
  public void validateCategoryCreated(Account account) {
    CreateStoryCategoryRequest createStoryCategoryRequest = account.getCreateStoryCategoryRequest();
    HttpResponse response = account.getLastResponse();

    response.statusMatches(HttpStatus.OK)
        .bodyMatches("data.krName", is(createStoryCategoryRequest.getKrName()))
        .bodyMatches("data.enName", is(createStoryCategoryRequest.getEnName()))
        .bodyMatches("data.priority", is(createStoryCategoryRequest.getPriority()))
        .bodyMatches("data.categoryId", not(isEmptyString()));

    account.setCreatedStoryCategory(response.getObject("data", StoryCategory.class));
  }

  @Step
  public void validateCategoryUpdated(Account account) {
    UpdateStoryCategoryRequest updateStoryCategoryRequest = account.getUpdateStoryCategoryRequest();

    HttpResponse response = account.getLastResponse();

    response.statusMatches(HttpStatus.OK)
        .bodyMatches("data.krName", is(updateStoryCategoryRequest.getKrName()))
        .bodyMatches("data.enName", is(updateStoryCategoryRequest.getEnName()))
        .bodyMatches("data.priority", is(updateStoryCategoryRequest.getPriority()))
        .bodyMatches("data.categoryId", not(isEmptyString()));

    account.setUpdatedStoryCategory(response.getObject("data", StoryCategory.class));
  }

  @Step
  public void validateCreatedStoryCategoryInList(Account account) {
    StoryCategory createdStoryCategory = account.getCreatedStoryCategory();

    StoryCategory storyCategoryInList = account.getStoryCategories().stream()
        .filter(category -> category.getCategoryId().equals(createdStoryCategory.getCategoryId()))
        .findAny()
        .orElseThrow(() -> new AssertionError("The created category is not on list"));

    assertThat(storyCategoryInList.getEnName(), is(createdStoryCategory.getEnName()));
    assertThat(storyCategoryInList.getKrName(), is(createdStoryCategory.getKrName()));
    assertThat(storyCategoryInList.getPriority(), is(createdStoryCategory.getPriority()));

  }

  @Step
  public void validateUpdatedStoryCategoryInList(Account account) {
    StoryCategory updatedStoryCategory = account.getUpdatedStoryCategory();

    StoryCategory storyCategoryInList = account.getStoryCategories().stream()
        .filter(category -> category.getCategoryId().equals(updatedStoryCategory.getCategoryId()))
        .findAny()
        .orElseThrow(() -> new AssertionError("The updated category is not on list"));

    assertThat(storyCategoryInList.getEnName(), is(updatedStoryCategory.getEnName()));
    assertThat(storyCategoryInList.getKrName(), is(updatedStoryCategory.getKrName()));
    assertThat(storyCategoryInList.getPriority(), is(updatedStoryCategory.getPriority()));
  }
}
