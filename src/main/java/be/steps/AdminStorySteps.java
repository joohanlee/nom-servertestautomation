package be.steps;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.isEmptyString;

import be.common.HttpRequest;
import be.common.HttpResponse;
import be.common.HttpStatus;
import be.common.OGQMarketURL.Content.Admin;
import be.dto.domains.Account;
import be.dto.domains.story.Story;
import be.dto.domains.story.StoryStatus;
import be.dto.request.story.CreateStoryAdminRequest;
import be.dto.request.story.UpdateStoryRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import java.util.List;
import java.util.stream.Collectors;
import net.thucydides.core.annotations.Step;

/**
 * Created by Joohan Lee on 2020/06/10
 */

public class AdminStorySteps {

  @Step
  public void create(Account account, String creatorId, String title, String content, String thumbnailImage, List<String> tags, StoryStatus status) {
    CreateStoryAdminRequest createStoryAdminRequest = CreateStoryAdminRequest.builder()
        .categoryId(account.getCreatedStoryCategory().getCategoryId())
        .title(title)
        .content(content)
        .thumbnailImage(thumbnailImage)
        .tags(tags)
        .creatorId(creatorId)
        .storyStatus(status)
        .build();

    HttpRequest request = HttpRequest.builder()
        .accessToken(account.getAccessToken())
        .requestURL(Admin.CREATE_STORY)
        .requestBody(createStoryAdminRequest)
        .build();

    HttpResponse response = request.post();

    account.setCreateStoryAdminRequest(createStoryAdminRequest);
    account.setLastResponse(response);
  }

  @Step
  public void update(Account account, String title, String content, String thumbnailImage, List<String> tags) {
    UpdateStoryRequest updateStoryRequest = UpdateStoryRequest.builder()
        .title(title)
        .content(content)
        .thumbnailImage(thumbnailImage)
        .tags(tags)
        .categoryId(account.getCreatedStoryCategory().getCategoryId())
        .build();

    HttpRequest request = HttpRequest.builder()
        .accessToken(account.getAccessToken())
        .requestURL(Admin.UPDATE_STORY)
        .pathParam("storyId", account.getCreatedStory().getStoryId())
        .requestBody(updateStoryRequest)
        .build();

    HttpResponse response = request.put();

    account.setUpdateStoryRequest(updateStoryRequest);
    account.setLastResponse(response);
  }

  @Step
  public void getList(Account account) {
    HttpRequest request = HttpRequest.builder()
        .accessToken(account.getAccessToken())
        .requestURL(Admin.GET_STORY_LIST)
        .build();

    HttpResponse response = request.get();

    account.setStories(response.getObject("data", new TypeReference<List<Story>>() {}));
    account.setLastResponse(response);
  }

  @Step
  public void delete(Account account, String storyId) {
    HttpRequest request = HttpRequest.builder()
        .accessToken(account.getAccessToken())
        .requestURL(Admin.DELETE_STORY)
        .pathParam("storyId", storyId)
        .build();

    request.delete()
        .statusMatches(HttpStatus.OK);
  }

  @Step
  public void validateStoryCreated(Account account) {
    CreateStoryAdminRequest createStoryAdminRequest = account.getCreateStoryAdminRequest();

    HttpResponse response = account.getLastResponse();

    response.statusMatches(HttpStatus.OK)
        .bodyMatches("data.title", is(createStoryAdminRequest.getTitle()))
        .bodyMatches("data.storyStatus", is(createStoryAdminRequest.getStoryStatus().name()))
        .bodyMatches("data.thumbnailImage", is(createStoryAdminRequest.getThumbnailImage()))
        .bodyMatches("data.storyId", not(isEmptyString()))
        .bodyMatches("data.category.categoryId", is(createStoryAdminRequest.getCategoryId()))
        .bodyMatches("data.tags", containsInAnyOrder(createStoryAdminRequest.getTags().toArray()));

    account.setCreatedStory(response.getObject("data", Story.class));
  }

  @Step
  public void validateStoryUpdated(Account account) {
    UpdateStoryRequest updateStoryRequest = account.getUpdateStoryRequest();

    HttpResponse response = account.getLastResponse();

    response.statusMatches(HttpStatus.OK)
        .bodyMatches("data.title", is(updateStoryRequest.getTitle()))
        .bodyMatches("data.thumbnailImage", is(updateStoryRequest.getThumbnailImage()))
        .bodyMatches("data.storyId", not(isEmptyString()))
        .bodyMatches("data.category.categoryId", is(updateStoryRequest.getCategoryId()))
        .bodyMatches("data.tags", containsInAnyOrder(updateStoryRequest.getTags().toArray()));

    account.setUpdatedStory(response.getObject("data", Story.class));
  }

  @Step
  public void validateCreatedStoryInDetail(Account account, Story story) {
    HttpRequest request = HttpRequest.builder()
        .accessToken(account.getAccessToken())
        .requestURL(Admin.GET_STORY_DETAIL)
        .pathParam("storyId", story.getStoryId())
        .build();

    Story storyFromResponse = request.get()
        .statusMatches(HttpStatus.OK)
        .getObject("data", Story.class);

    assertThat(storyFromResponse.getStoryId(), is(story.getStoryId()));
    assertThat(storyFromResponse.getTitle(), is(story.getTitle()));
    assertThat(storyFromResponse.getContent(), is(story.getContent()));
    assertThat(storyFromResponse.getThumbnailImage(), is(story.getThumbnailImage()));
    assertThat(storyFromResponse.getStoryStatus(), is(story.getStoryStatus()));
    assertThat(storyFromResponse.getCategory().getCategoryId(), is(story.getCategory().getCategoryId()));
    assertThat(storyFromResponse.getTags().size(), is(story.getTags().size()));
    assertThat(storyFromResponse.getTags()
            .stream()
            .map(s -> s.replaceAll("\\\\", ""))
            .collect(Collectors.toList()),
        containsInAnyOrder(story.getTags().toArray()));
  }
}
