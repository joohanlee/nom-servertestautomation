package automation.be.definitions;

import au.com.bytecode.opencsv.CSVReader;
import be.common.TestContext;
import be.dto.domains.Account;
import be.steps.AdminArtworkSteps;
import cucumber.api.java.en.When;
import java.io.FileReader;
import java.io.IOException;
import net.thucydides.core.annotations.Steps;
import org.springframework.core.io.ClassPathResource;

/**
 * Created by Joohan Lee on 2019/11/27
 */


public class AdminArtworkStepDefinition {
  @Steps
  private AdminArtworkSteps adminArtworkSteps;

  @When("the account {string} changes the category of artworks in the file to {string}")
  public void changeArtworkCategoryInFile(String accountKey, String categoryId) {
    Account account = TestContext.getAccount(accountKey);

    try {
      CSVReader csvReader = new CSVReader(new FileReader(new ClassPathResource("tmp.csv").getFile()));

      String[] tmp = csvReader.readNext();

      System.out.println("Column name: " + tmp[0]);

      while((tmp = csvReader.readNext()) != null) {
        adminArtworkSteps.changeCategory(account, tmp[0], categoryId);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
