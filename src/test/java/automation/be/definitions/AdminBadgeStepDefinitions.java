package automation.be.definitions;

import be.common.TestContext;
import be.dto.domains.Account;
import be.dto.domains.badge.BadgeAwardType;
import be.dto.domains.badge.BadgeConditionCheck;
import be.dto.domains.badge.BadgeConditionType;
import be.dto.request.badge.CreateBadgeConditionRequestBody;
import be.dto.request.badge.CreateBadgeRequestBody;
import be.steps.AdminBadgeSteps;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.Arrays;
import net.thucydides.core.annotations.Steps;

/**
 * Created by Joohan Lee on 2019-10-15
 */
public class AdminBadgeStepDefinitions {
  @Steps
  private AdminBadgeSteps adminBadgeSteps;

  @When("the account {string} creates a badge with {string}, {string}, {string}, {int}, {}, {string}, {string}, {}, {}, {string} on admin page")
  public void createBadge(
      String accountKey,
      String name,
      String descriptionForCreator,
      String descriptionForMarket,
      int priority,
      BadgeAwardType badgeAwardType,
      String badgeFileKey,
      String disabledBadgeFileKey,
      BadgeConditionType badgeConditionType,
      BadgeConditionCheck badgeConditionCheck,
      String requiredValue
  ) {
    Account account = TestContext.getAccount(accountKey);

    CreateBadgeConditionRequestBody createBadgeConditionRequestBody = CreateBadgeConditionRequestBody.builder()
        .badgeConditionCheck(badgeConditionCheck)
        .badgeConditionType(badgeConditionType)
        .requiredValue(requiredValue)
        .build();

    CreateBadgeRequestBody createBadgeRequestBody = CreateBadgeRequestBody.builder()
        .badgeConditions(Arrays.asList(createBadgeConditionRequestBody))
        .badgeFileKey(badgeFileKey)
        .disabledBadgeFileKey(disabledBadgeFileKey)
        .name(name)
        .descriptionForCreator(descriptionForCreator)
        .descriptionForMarket(descriptionForMarket)
        .badgeAwardType(badgeAwardType)
        .priority(priority)
        .build();

    adminBadgeSteps.createBadge(account, createBadgeRequestBody);
  }

  @When("the account {string} deletes the created badge")
  public void validateCreatedBadgeInList(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    adminBadgeSteps.deleteBadge(account, account.getCreatedBadgeId());
  }

  @When("the account {string} gets all badges on admin page")
  public void getBadges(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    adminBadgeSteps.getBadges(account);
  }

  @Then("the account {string} validates the badge created response on admin page")
  public void validateBadgeCreatedResponse(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    adminBadgeSteps.validateBadgeCreatedResponse(account);
  }
  @Then("the account {string} validates the created badge is in the response badge list on admin page")
  public void validateCreatedBadgeInResponse(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    adminBadgeSteps.validateBadgeInTheList(account, account.getCreatedBadgeId());
  }
}
