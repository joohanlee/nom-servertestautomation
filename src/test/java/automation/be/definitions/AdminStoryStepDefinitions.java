package automation.be.definitions;

import be.common.TestContext;
import be.dto.domains.Account;
import be.dto.domains.story.StoryStatus;
import be.steps.AdminStorySteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.Arrays;
import java.util.List;
import net.thucydides.core.annotations.Steps;

/**
 * Created by Joohan Lee on 2020/06/10
 */
public class AdminStoryStepDefinitions {
  @Steps
  AdminStorySteps adminStorySteps;

  @Given("the account {string} cleaned up all stories having the title starting with title {string}")
  public void deleteStoryTitleStartingWith(String accountKey, String title) {
    Account account = TestContext.getAccount(accountKey);

    adminStorySteps.getList(account);

    account.getStories()
        .stream()
        .filter(story -> story.getTitle().startsWith(title))
        .forEach(story -> adminStorySteps.delete(account, story.getStoryId()));
  }


  @When("the account {string} creates a story of the creator {string} with {string}, {string}, {string}, {string}, {}")
  public void create(String accountKey1, String accountKey2, String title, String content, String thumbnailImage, String tags, StoryStatus storyStatus) {
    Account account1 = TestContext.getAccount(accountKey1);
    Account account2 = TestContext.getAccount(accountKey2);

    List<String> tagsAsList = Arrays.asList(tags.split(","));

    adminStorySteps.create(account1, account2.getUserId(), title, content, thumbnailImage, tagsAsList, storyStatus);
  }

  @When("the account {string} gets story list")
  public void getStories(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    adminStorySteps.getList(account);
  }

  @When("the account {string} updates the created story with {string}, {string}, {string}, {string}")
  public void updateCreatedStory(String accountKey, String updatedTitle, String updatedContent, String updatedThumbnailImage, String updatedTags) {
    Account account = TestContext.getAccount(accountKey);
    List<String> tagsInList = Arrays.asList(updatedTags.split(","));

    adminStorySteps.update(account, updatedTitle, updatedContent, updatedThumbnailImage, tagsInList);
  }

  @Then("the account {string} validates the story is created")
  public void validateStoryCreated(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    adminStorySteps.validateStoryCreated(account);
  }

  @Then("the account {string} validates the story is updated")
  public void validateStoryUpdated(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    adminStorySteps.validateStoryUpdated(account);
  }

  @Then("the account {string} validates the created story in detail")
  public void validateCreatedStoryInDetail(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    adminStorySteps.validateCreatedStoryInDetail(account, account.getCreatedStory());
  }

  @Then("the account {string} validates the updated story in detail")
  public void validateUpdatedStoryInDetail(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    adminStorySteps.validateCreatedStoryInDetail(account, account.getUpdatedStory());
  }
}
