package automation.be.definitions;

import be.common.TestContext;
import be.dto.domains.Account;
import be.steps.CustomerBadgeSteps;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

/**
 * Created by Joohan Lee on 2019-10-15
 */
public class CustomerBadgeStepDefinitions {

  @Steps
  CustomerBadgeSteps customerBadgeSteps;

  @When("the account {string} gets creator {string} badges on customer page")
  public void getCreatorBadges(String accountKey1, String accountKey2) {
    Account customer = TestContext.getAccount(accountKey1);
    Account creator = TestContext.getAccount(accountKey2);

    customerBadgeSteps.getCreatorBadges(customer, creator.getUserId());
  }

  @Then("the account {string} validates creator badges response on customer page")
  public void validateCreatorBadges(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    customerBadgeSteps.validateCreatorBadges(account);
  }
}
