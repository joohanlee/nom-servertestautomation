package automation.be.definitions;

import be.common.TestContext;
import be.dto.domains.Account;
import be.dto.domains.badge.BadgeConditionType;
import be.dto.response.badge.CreatorBadgeResponse;
import be.steps.CreatorBadgeSteps;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

/**
 * Created by Joohan Lee on 2019-10-15
 */
public class CreatorBadgeStepDefinitions {
  @Steps
  CreatorBadgeSteps creatorBadgeSteps;

  @When("the account {string} gets badge list on creator page")
  public void getBadgeList(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    creatorBadgeSteps.getCreatorBadges(account);
  }

  @When("the account {string} get a badge detail of type {} on creator page")
  public void getBadgeDetail(String accountKey, BadgeConditionType badgeConditionType) {
    Account account = TestContext.getAccount(accountKey);
    String badgeId = account.getCreatorBadgeListResponse()
        .get(badgeConditionType)
        .stream()
        .map(CreatorBadgeResponse::getBadgeId)
        .findAny()
        .orElseThrow(() -> new AssertionError("Failed"));

    creatorBadgeSteps.getBadgeDetail(account, badgeId);
  }

  @Then("the account {string} validates badge list response on creator page")
  public void validateBadges(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    creatorBadgeSteps.validateBadgeListResponse(account);
  }
}
