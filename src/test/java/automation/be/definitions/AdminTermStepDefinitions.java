package automation.be.definitions;

import be.common.TestContext;
import be.dto.domains.Account;
import be.dto.request.term.CreateTermRequestBody;
import be.enums.Language;
import be.enums.TermType;
import be.steps.AdminTermSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.apache.commons.lang3.RandomUtils;

public class AdminTermStepDefinitions {
    @Steps
    AdminTermSteps adminTermSteps;

    @When("the account {string} searches user term by {string}, {word}, {}, {int}, {int} on user term page")
    public void searchOnTerms(String accountKey, String name, String activated, TermType termType, Integer page, Integer pageSize) {
        Account account = TestContext.getAccount(accountKey);

        adminTermSteps.searchOnTerm(account, name, activated, termType, page, pageSize);
    }


    @Given("the account {string} created a user term with {string}, {int}, {}, {word} on admin page")
    @When("the account {string} creates a user term with {string}, {int}, {}, {word} on admin page")
    public void createTerm(String accountKey, String name, int displayOrder, TermType termType, String activated) {
        Account account = TestContext.getAccount(accountKey);
        final String randomTermId = new StringBuilder()
            .append(termType.toString())
            .append("-")
            .append(RandomUtils.nextInt(10000,99999))
            .toString();

        adminTermSteps.createTerm(account, randomTermId, name, displayOrder, termType, Boolean.valueOf(activated));
    }

    @When("the account {string} creates a duplicated user term on admin page")
    public void createDuplicateTerm(String accountKey) {
        Account account = TestContext.getAccount(accountKey);
        CreateTermRequestBody createdTermRequestBody = account.getCreatedTermRequestBody();

        adminTermSteps.createTerm(
            account,
            createdTermRequestBody.getTermId(),
            createdTermRequestBody.getName(),
            createdTermRequestBody.getDisplayOrder(),
            createdTermRequestBody.getTermType(),
            createdTermRequestBody.isActivated()
        );
    }

    @Given("the account {string} created a content for the created user term with {string}, {string}, {} on admin page")
    @When("the account {string} creates a content for the created user term with {string}, {string}, {} on admin page")
    public void createTermContent(String accountKey, String title, String content, Language language) {
        Account account = TestContext.getAccount(accountKey);
        String termId = account.getCreatedTermRequestBody().getTermId();

        adminTermSteps.createTermContent(account, termId, title, content, language);
    }

    @When("the account {string} gets a term from {string} on admin page")
    public void getTerm(String accountKey, String termId) {
        Account account = TestContext.getAccount(accountKey);

        adminTermSteps.getTerm(account, termId);
    }

    @When("the account {string} gets a term content from {string} on admin page")
    public void getTermContent(String accountKey, String termId) {
        Account account = TestContext.getAccount(accountKey);

        adminTermSteps.getTerm(account, termId);
    }

    @When("the account {string} creates a content for the user term from {string} with {string}, {string}, {} on admin page")
    public void createTermContent(String accountKey, String termId, String title, String content, Language language) {
        Account account = TestContext.getAccount(accountKey);

        adminTermSteps.createTermContent(account, termId, title, content, language);
    }

    @When("the account {string} updates the created user term to have {string}, {int}, {}, {word} on admin page")
    public void updateTerm(String accountKey, String name, int displayOrder, TermType termType, String activated) {
        Account account = TestContext.getAccount(accountKey);

        adminTermSteps.updateTerm(
            account,
            account.getCreatedTermRequestBody().getTermId(),
            name,
            displayOrder,
            termType,
            Boolean.valueOf(activated)
        );
    }

    @When("the account {string} updates the user term from {string} with {string}, {int}, {}, {word} on admin page")
    public void updateTerm(String accountKey, String termId, String name, int displayOrder, TermType termType, String activated) {
        Account account = TestContext.getAccount(accountKey);

        adminTermSteps.updateTerm(
            account,
            termId,
            name,
            displayOrder,
            termType,
            Boolean.valueOf(activated)
        );
    }

    @When("the account {string} updates the content for created user term to have {string}, {string}, {} on admin page")
    public void updateTermContent(String accountKey, String title, String content, Language language) {
        Account account = TestContext.getAccount(accountKey);

        adminTermSteps.updateTermContent(
            account,
            account.getCreatedTermRequestBody().getTermId(),
            title,
            content,
            language
        );

    }

    @Then("the account {string} should receive terms filtered by {word}, {}, {int} on user term page")
    public void validateSearchTerm(String accountKey, String activated, TermType termType, int limit) {
        Account account = TestContext.getAccount(accountKey);

        adminTermSteps.validateSearchTermResult(account, activated, termType, limit);
    }

    @Then("the account {string} validates the created user term on admin page")
    public void validateCreateUserTerm(String accountKey) {
        Account account = TestContext.getAccount(accountKey);
        final String createdTermId = account.getCreatedTermRequestBody().getTermId();

        adminTermSteps.getTerm(account, createdTermId);
        adminTermSteps.validateCreateTermByGetTermResponse(account);
    }

    @Then("the account {string} validates the updated user term on admin page")
    public void validateUpdateUserTerm(String accountKey) {
        Account account = TestContext.getAccount(accountKey);

        adminTermSteps.getTerm(account, account.getCreatedTermRequestBody().getTermId());
        adminTermSteps.validateUpdateTermByGetTermResponse(account);
    }

    @Then("the account {string} validates the content is created for the created term on admin page")
    public void validateCreateTermContent(String accountKey) {
        Account account = TestContext.getAccount(accountKey);

        adminTermSteps.getTermContent(account, account.getCreatedTermRequestBody().getTermId());
        adminTermSteps.validateCreatedTermContent(account);
    }

    @Then("the account {string} validates the updated user term content on admin page")
    public void validateUpdateUserTermContent(String accountKey) {
        Account account = TestContext.getAccount(accountKey);

        adminTermSteps.getTermContent(account, account.getCreatedTermRequestBody().getTermId());
        adminTermSteps.validateUpdateTermContentByGetTermContentResponse(account);
    }
}
