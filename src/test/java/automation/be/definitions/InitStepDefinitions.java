package automation.be.definitions;

import be.common.TestContext;
import cucumber.api.java.Before;
import io.restassured.RestAssured;

public class InitStepDefinitions {
    @Before
    public void beforeTest() {
        RestAssured.baseURI = TestContext.getSystemVariable("baseurl");
        RestAssured.port = TestContext.getSystemVariableAsInt("baseport");
    }
}
