package automation.be.definitions;

import be.common.TestContext;
import be.dto.domains.Account;
import be.steps.AdminStoryCategorySteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

/**
 * Created by Joohan Lee on 2020/06/08
 */
public class AdminStoryCategoryStepDefinitions {

  @Steps
  private AdminStoryCategorySteps adminStoryCategorySteps;

  @Given("the account {string} cleaned up all story categories starting with English name {string}")
  public void deleteAllStoryCategories(String accountKey, String enName) {
    Account account = TestContext.getAccount(accountKey);

    adminStoryCategorySteps.getList(account);
    adminStoryCategorySteps.deleteEnNameStartingWith(account, enName);
  }

  @Given("the account {string} created a new story category for story test")
  public void createdStoryCategory(String accountKey) {
    final String enName = "Test Automation";
    final String krName = "테스트 자동화";
    final int priority = 2;

    Account account = TestContext.getAccount(accountKey);

    adminStoryCategorySteps.getList(account);
    adminStoryCategorySteps.deleteEnNameStartingWith(account, enName);
    adminStoryCategorySteps.create(account, krName, enName, priority);
    adminStoryCategorySteps.validateCategoryCreated(account);
  }


  @When("the account {string} creates a story category with {string}, {string}, {int}")
  public void createStoryCategory(String accountKey, String krName, String enName, int priority) {
    Account account = TestContext.getAccount(accountKey);

    krName = checkNullString(krName);
    enName = checkNullString(enName);

    adminStoryCategorySteps.create(account, krName, enName, priority);
  }

  @When("the account {string} gets story category list")
  public void getStoryCategoryList(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    adminStoryCategorySteps.getList(account);
  }
  @When("the account {string} updates the created story category with {string}, {string}, {int}")
  public void updateCreatedStoryCategory(String accountKey, String krName, String enName, int priority) {
    Account account = TestContext.getAccount(accountKey);

    adminStoryCategorySteps.updateCreatedStoryCategory(account, krName, enName, priority);
  }

  @When("the account {string} deletes the created story category")
  public void deleteCreatedStoryCategory(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    adminStoryCategorySteps.delete(account, account.getCreatedStoryCategory().getCategoryId());

  }

  @Then("the account {string} validates the story category is created")
  public void validateStoryCategoryCreated(String accountKey) {
    Account account = TestContext.getAccount(accountKey);
    adminStoryCategorySteps.validateCategoryCreated(account);
  }

  @Then("the account {string} validates the story category is updated")
  public void validateStoryCategoryUpdated(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    adminStoryCategorySteps.validateCategoryUpdated(account);
  }

  @Then("the account {string} validates the created story category is in the list")
  public void validateCreatedStoryCategoryInList(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    adminStoryCategorySteps.validateCreatedStoryCategoryInList(account);
  }

  @Then("the account {string} validates the updated story category is in the list")
  public void validateUpdatedStoryCategoryInList(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    adminStoryCategorySteps.validateUpdatedStoryCategoryInList(account);
  }

  private String checkNullString(String s) {
    final String nullString = "null";

    if (s.equals(nullString)) {
      return null;
    }

    return s;
  }
}