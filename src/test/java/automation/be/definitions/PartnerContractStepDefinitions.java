package automation.be.definitions;

import be.common.TestContext;
import be.dto.domains.Account;
import be.steps.PartnerContractSteps;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

/**
 * Created by Joohan Lee on 2019/10/30
 */
public class PartnerContractStepDefinitions {
  @Steps
  PartnerContractSteps partnerContractSteps;

  @When("the account {string} creates a partner contract for the creator {string} with {word}")
  public void createPartnerContract(String accountKey1, String accountKey2, String isMassUploader) {
    Account admin = TestContext.getAccount(accountKey1);
    Account creator = TestContext.getAccount(accountKey2);

    partnerContractSteps.createPartnerContract(admin, creator.getUserId(), Boolean.valueOf(isMassUploader));
  }
  @When("the account {string} gets partner contract list")
  public void getPartnerContracts(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    partnerContractSteps.getPartnerContracts(account);
  }

  @When("the account {string} delete all partner contracts")
  public void deleteAllPartnerContracts(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    partnerContractSteps.deleteAllPartnerContracts(account);
  }

  @When("the account {string} updates the first partner contract in the list with creatorId {} and massUploader {word}")
  public void updateFirstPartnerContract(String accountKey, String updatedCreatorId, String isMassUploader) {
    Account account = TestContext.getAccount(accountKey);

    partnerContractSteps.updatePartnerContract(account, updatedCreatorId, Boolean.valueOf(isMassUploader));
  }

  @Then("the account {string} validates the first partner contract in the list has creatorId {} and massUploader {word}")
  public void validateFirstPartnerContract(String accountKey, String creatorId, String isMassUploader) {
    Account account = TestContext.getAccount(accountKey);

    partnerContractSteps.validateFirstPartnerContract(account, creatorId, Boolean.valueOf(isMassUploader));
  }

  @Then("the account {string} validates a partner contract is created")
  public void validatePartnerContractCreatedResponse(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    partnerContractSteps.validatePartnerContractCreated(account);
  }

  @Then("the account {string} validates the created partner contract is in the partner contract list")
  public void validateCreatedPartnerContractInList(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    partnerContractSteps.validateCreatedPartnerContractInList(account);
  }

  @Then("the account {string} validates partner contract list is empty")
  public void validatePartnerContractEmpty(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    partnerContractSteps.validatePartnerContractEmpty(account);
  }

  @Then("the account {string} cleans up all partner contracts")
  public void cleanUpPartnerContracts(String accountKey) {
    Account account = TestContext.getAccount(accountKey);

    partnerContractSteps.getPartnerContracts(account);
    partnerContractSteps.deleteAllPartnerContracts(account);
    partnerContractSteps.getPartnerContracts(account);
    partnerContractSteps.validatePartnerContractEmpty(account);
  }
}
