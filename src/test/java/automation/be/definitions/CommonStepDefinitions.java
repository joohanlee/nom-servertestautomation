package automation.be.definitions;

import be.common.HttpStatus;
import be.common.TestContext;
import be.dto.domains.Account;
import be.enums.ResponseCode;
import be.steps.CommonSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class CommonStepDefinitions {
    //TODO: 실제 계정 생성 후 로그인 플로우 타기
    //TODO: Token을 실제 정보를 가지고 만들어주기

    //TODO: ACCESS TOKEN 환경 변수로 받아오기
    private static String adminToken = TestContext.getSystemVariable("adminToken");
    private static String creatorToken = TestContext.getSystemVariable("creatorToken");
    private static String customerToken = TestContext.getSystemVariable("customerToken");

    @Steps
    CommonSteps commonSteps;

    @Given("the admin {string} logs in")
    public void adminLogin(String accountKey) {
        Account account = Account.builder()
            .accessToken(adminToken)
            .build();

        TestContext.setAccount(accountKey, account);
    }

    @Given("the creator {string} logs in")
    public void creatorLogin(String accountKey) {
        Account account = Account.builder()
            .userId("591b45d84f2e4")
            .accessToken(creatorToken)
            .build();

        TestContext.setAccount(accountKey, account);
    }
    @Given("the customer {string} logs in")
    public void customerLogin(String accountKey) {
        Account account = Account.builder()
            .accessToken(customerToken)
            .build();

        TestContext.setAccount(accountKey, account);
    }


    @Then("the account {string} receives a status code {}")
    public void validateErrorResponseStatus(String accountKey, HttpStatus httpStatus) {
        Account account = TestContext.getAccount(accountKey);

        commonSteps.validateResponseStatus(account.getLastResponse(), httpStatus);
    }

    @Then("the account {string} receives a status code {} and a response code {}")
    public void validateErrorResponse(String accountKey, HttpStatus httpStatus, ResponseCode responseCode) {
        Account account = TestContext.getAccount(accountKey);

        commonSteps.validateResponseCode(account.getLastResponse(), httpStatus, responseCode);
    }
}
