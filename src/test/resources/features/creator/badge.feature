@feature_badge
  Feature: Test on badge in creator site

  Scenario: Get creator's badges
    Given the creator "account1" logs in
    When the account "account1" gets badge list on creator page
    Then the account "account1" validates badge list response on creator page

  Scenario Outline: Get creator's badge detail
    Given the creator "account1" logs in
    When the account "account1" gets badge list on creator page
    When the account "account1" get a badge detail of type <badgeConditionType> on creator page
    Then the account "account1" receives a status code OK and a response code OK

    Examples:
      |badgeConditionType    |
      |MESSAGE               |
      |STICKER_ON_SALE       |
      |STOCK_IMAGE_ON_SALE   |
      |AUDIO_ON_SALE         |
      |COLORING_SHEET_ON_SALE|
      |FAN                   |
      |LEVEL                 |
      |GOODS                 |
      |CHEER_COMMENT         |
      |FEATURED_CREATOR      |
