@feature_badge
Feature: Test on badge in customer site

  Scenario: Get creator's badges from customer
    Given the customer "account1" logs in
    And the creator "account2" logs in
    When the account "account1" gets creator "account2" badges on customer page
    Then the account "account1" validates creator badges response on customer page

