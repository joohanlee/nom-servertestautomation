@feature_story_category
Feature: Test on story category apis in admin site
  Scenario Outline: Create & update a story category
    Given the admin "account1" logs in
    And the account "account1" cleaned up all stories having the title starting with title "Test Auto"
    And the account "account1" cleaned up all story categories starting with English name "Test Auto"
    When the account "account1" creates a story category with <krName>, <enName>, <priority>
    Then the account "account1" validates the story category is created
    When the account "account1" gets story category list
    Then the account "account1" validates the created story category is in the list
    When the account "account1" updates the created story category with <updatedKrName>, <updatedEnName>, <updatedPriority>
    Then the account "account1" validates the story category is updated
    When the account "account1" gets story category list
    Then the account "account1" validates the updated story category is in the list

    Examples:
      |krName     |enName           |priority|updatedKrName     |updatedEnName              |updatedPriority|
      |"테스트 자동화"|"Test Automation"|1       |"테스트 자동화-업데이트"|"Test Automation - updated"|2              |

  @run_this
  Scenario Outline: Create an invalid story category
    Given the admin "account1" logs in
    And the account "account1" cleaned up all stories having the title starting with title "Test Auto"
    And the account "account1" cleaned up all story categories starting with English name "Test Auto"
    When the account "account1" creates a story category with <krName>, <enName>, <priority>
    Then the account "account1" receives a status code BAD_REQUEST

    Examples:
      |krName     |enName           |priority|
      |"테스트 자동화"|"Test Automation"|0       |
      |"테스트 자동화"|""               |1       |
      |""         |"Test Automation"|1       |
      |"null"     |"Test Automation"|1       |
      |"         "|"Test Automation"|1       |
      |"테스트 자동화"|"null"           |1       |


  Scenario Outline: Create a story category having duplicated name
    Given the admin "account1" logs in
    And the account "account1" cleaned up all stories having the title starting with title "Test Auto"
    And the account "account1" cleaned up all story categories starting with English name "Test Automation"
    When the account "account1" creates a story category with <krNameFirst>, <enNameFirst>, <priorityFirst>
    Then the account "account1" validates the story category is created
    When the account "account1" creates a story category with <krNameSecond>, <enNameSecond>, <prioritySecond>
    Then the account "account1" receives a status code CONFLICT and a response code <errorCode>

    Examples:
      |krNameFirst   |enNameFirst      |priorityFirst|krNameSecond   |enNameSecond      |prioritySecond|errorCode|
      |"테스트 자동화-중복"|"Test Automation-duplicated"|1|"테스트 자동화-중복"|"Test Automation-duplicated"|1|DUPLICATED_STORY_CATEGORY_NAME|
      |"테스트 자동화-중복"|"Test Automation-duplicated"|1|"테스트 자동화-중복"|"Test Automation-nonDuplicated"|2|DUPLICATED_STORY_CATEGORY_NAME|
      |"테스트 자동화-중복"|"Test Automation-duplicated"|1|"테스트 자동화-중복안됨"|"Test Automation-duplicated"|2|DUPLICATED_STORY_CATEGORY_NAME|

  Scenario Outline: Update a story category to have duplicated name
    Given the admin "account1" logs in
    And the account "account1" cleaned up all story categories starting with English name "Test Automation"
    When the account "account1" creates a story category with <krNameFirst>, <enNameFirst>, <priorityFirst>
    Then the account "account1" validates the story category is created
    When the account "account1" creates a story category with <krNameSecond>, <enNameSecond>, <prioritySecond>
    Then the account "account1" validates the story category is created
    When the account "account1" updates the created story category with <krNameFirst>, <enNameFirst>, <priorityFirst>
    Then the account "account1" receives a status code CONFLICT and a response code <errorCode>

    Examples:
      |krNameFirst    |enNameFirst      |priorityFirst|krNameSecond   |enNameSecond      |prioritySecond|errorCode|
      |"테스트 자동화-첫번째"|"Test Automation-first"|1|"테스트 자동화-두번째"|"Test Automation-second"|1|DUPLICATED_STORY_CATEGORY_NAME|

  Scenario Outline: Delete a story category containing a story
    Given the admin "account1" logs in
    And the creator "account2" logs in
    And the account "account1" cleaned up all stories having the title starting with title <title>
    And the account "account1" created a new story category for story test
    When the account "account1" creates a story of the creator "account2" with <title>, <content>, <thumbnailImage>, <tags>, <storyStatus>
    Then the account "account1" validates the story is created
    When the account "account1" deletes the created story category
    Then the account "account1" receives a status code BAD_REQUEST and a response code ERROR

    Examples:
      |title            |content          |thumbnailImage|tags                       |storyStatus|
      |"Test Automation"|"Test Automation"|"testImageUrl"|"test,automation,temporary"|PUBLISHED  |

