Feature: Test on term management in admin site

  Scenario Outline: Search by user terms
    Given the admin "account1" logs in
    When the account "account1" searches user term by <termName>, <activated>, <termType>, <page>, <pageSize> on user term page
    Then the account "account1" should receive terms filtered by <activated>, <termType>, <pageSize> on user term page

    Examples:
      |termName|activated|termType|page|pageSize|
      |""|true|USER   |0|20|
      |""|true|CREATOR|0|20|
      |""|true|GRAFOLIO|0|20|

  Scenario Outline: Create a duplicated user term
    Given the admin "account1" logs in
    When the account "account1" creates a user term with <name>, <displayOrder>, <termType>, <activated> on admin page
    Then the account "account1" validates the created user term on admin page
    When the account "account1" creates a duplicated user term on admin page
    Then the account "account1" receives a status code <statusCode> and a response code <errorCode>

    Examples:
      |name|displayOrder|termType|activated|statusCode|errorCode|
      |"testAuto"|1       |USER    |true   |CONFLICT  |ACCOUNT_ALREADY_EXISTS_TERM|
      |"testAuto"|1       |CREATOR |true   |CONFLICT  |ACCOUNT_ALREADY_EXISTS_TERM|
      |"testAuto"|1       |GRAFOLIO|true   |CONFLICT  |ACCOUNT_ALREADY_EXISTS_TERM|

  Scenario Outline: Create a duplicated user term content
    Given the admin "account1" logs in
    And the account "account1" created a user term with <name>, <displayOrder>, <termType>, <activated> on admin page
    When the account "account1" creates a content for the created user term with <title>, <content>, <language> on admin page
    Then the account "account1" validates the content is created for the created term on admin page
    When the account "account1" creates a content for the created user term with <title>, <content>, <language> on admin page
    Then the account "account1" receives a status code <statusCode> and a response code <errorCode>

    Examples:
      |name      |displayOrder|termType|activated|title                         |content                 |language|statusCode|errorCode|
      |"testAuto"|1           |USER    |true     |"Automated term content title"|"This is auto generated"|EN      |CONFLICT  |ACCOUNT_ALREADY_EXISTS_TERM_CONTENT|

  Scenario Outline: Update a user term and content
    Given the admin "account1" logs in
    And the account "account1" created a user term with <name>, <displayOrder>, <termType>, <activated> on admin page
    And the account "account1" created a content for the created user term with <title>, <content>, <language> on admin page
    When the account "account1" updates the created user term to have <name>, <displayOrder>, <termType>, <activated> on admin page
    Then the account "account1" validates the updated user term on admin page
    When the account "account1" updates the content for created user term to have <title>, <content>, <language> on admin page
    Then the account "account1" validates the updated user term content on admin page

    Examples:
      |name      |displayOrder|termType|activated|title                                          |content                                            |language|
      |"testAuto"|1           |USER    |true     |"Updated term content title by test automation"|"This content should be updated by test automation"|EN      |


  Scenario Outline: Create a content for not existing user term
    Given the admin "account1" logs in
    When the account "account1" creates a content for the user term from "NOT_EXISTING_TERM_ID" with <title>, <content>, <language> on admin page
    Then the account "account1" receives a status code NOT_FOUND and a response code NOT_EXIST_TERM

    Examples:
      |title                         |content                 |language|
      |"Automated term content title"|"This is auto generated"|EN      |

  Scenario: Get a not existing user term
    Given the admin "account1" logs in
    When the account "account1" gets a term from "NOT_EXISTING_TERM_ID" on admin page
    Then the account "account1" receives a status code NOT_FOUND and a response code NOT_EXIST_TERM

  Scenario: Get a content for not existing user term
    Given the admin "account1" logs in
    When the account "account1" gets a term content from "NOT_EXISTING_TERM_ID" on admin page
    Then the account "account1" receives a status code NOT_FOUND and a response code NOT_EXIST_TERM

  Scenario Outline: Update not existing user term and user term content
    Given the admin "account1" logs in
    And the account "account1" created a user term with <name>, <displayOrder>, <termType>, <activated> on admin page
    When the account "account1" updates the user term from "NOT_EXIST_TERM" with <name>, <displayOrder>, <termType>, <activated> on admin page
    Then the account "account1" receives a status code NOT_FOUND and a response code NOT_EXIST_TERM
    When the account "account1" updates the content for created user term to have <title>, <content>, <language> on admin page
    Then the account "account1" receives a status code NOT_FOUND and a response code NOT_EXIST_TERM_CONTENT

    Examples:
      |name      |displayOrder|termType|activated|title                         |content                 |language|
      |"testAuto"|1           |USER    |true     |"Automated term content title"|"This is auto generated"|EN|
