@feature_story
Feature: Test on story apis in admin site

  Scenario Outline: Create & update a story
    Given the admin "account1" logs in
    And the creator "account2" logs in
    And the account "account1" cleaned up all stories having the title starting with title <title>
    And the account "account1" created a new story category for story test
    When the account "account1" creates a story of the creator "account2" with <title>, <content>, <thumbnailImage>, <tags>, <storyStatus>
    Then the account "account1" validates the story is created
    And the account "account1" validates the created story in detail
    When the account "account1" updates the created story with <updatedTitle>, <updatedContent>, <updatedThumbnailImage>, <updatedTags>
    Then the account "account1" validates the story is updated
    And the account "account1" validates the updated story in detail

    Examples:
      |title            |content          |thumbnailImage|tags                       |storyStatus|updatedTitle             |updatedContent    |updatedThumbnailImage|updatedTags|
      |"Test Automation"|"Test Automation"|"testImageUrl"|"test,automation,temporary"|PUBLISHED  |"Test Automation-updated"|"Test Automation-updated"|"updatedTestImageUrl"|"updated,test,automation"|
