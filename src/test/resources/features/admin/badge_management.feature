@feature_badge
Feature: Test on badge management in admin site

  Scenario Outline: Create a badge
    Given the admin "account1" logs in
    When the account "account1" creates a badge with <name>, <descriptionForCreator>, <descriptionForMarket>, <priority>, <badgeAwardType>, <badgeFileKey>, <disabledBadgeFileKey>, <badgeConditionType>, <badgeConditionCheck>, <requiredValue> on admin page
    Then the account "account1" validates the badge created response on admin page
    When the account "account1" gets all badges on admin page
    Then the account "account1" validates the created badge is in the response badge list on admin page
    When the account "account1" deletes the created badge
    Then the account "account1" receives a status code OK and a response code OK

    Examples:
      |name      |descriptionForCreator        |descriptionForMarket|priority|badgeAwardType|badgeFileKey                                  |disabledBadgeFileKey                          |badgeConditionType|badgeConditionCheck  |requiredValue|
      |"메세지 뱃지"|"보낸 메세지가 1개 이상이면 주는 뱃지"|"마켓용 설명입니다"     |1       |AUTO          |"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|MESSAGE           |GREATER_THAN_OR_EQUAL|"1"          |
      |"스티커 판매 뱃지"|"판매중인 스티커가 1개 이상이면 주는 뱃지"|"마켓용 설명입니다"     |1       |AUTO          |"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|STICKER_ON_SALE           |GREATER_THAN_OR_EQUAL|"1"          |
      |"오디오 판매 뱃지"|"판매중인 오디오가 1개 이상이면 주는 뱃지"|"마켓용 설명입니다"     |1       |AUTO          |"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|AUDIO_ON_SALE           |GREATER_THAN_OR_EQUAL|"1"          |
      |"컬러링 시트 판매 뱃지"|"판매중인 컬러링 시트가 1개 이상이면 주는 뱃지"|"마켓용 설명입니다"     |1       |AUTO          |"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|COLORING_SHEET_ON_SALE           |GREATER_THAN_OR_EQUAL|"1"          |
      |"팬 뱃지"|"팬이 1개 이상이면 주는 뱃지"|"마켓용 설명입니다"     |1       |AUTO          |"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|FAN           |GREATER_THAN_OR_EQUAL|"1"          |
      |"VIP 뱃지"|"VIP에게 주는 뱃지"|"마켓용 설명입니다"     |1       |AUTO          |"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|LEVEL|GREATER_THAN_OR_EQUAL|"VIP"          |
      |"상품화"|"상품화 완료시 주는 뱃지"|"마켓용 설명입니다"     |1       |AUTO          |"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|GOODS|EQUAL|"COMPLETE" |
      |"응원하기"|"응원하기 1개 이상이면 주는 뱃지"|"마켓용 설명입니다"     |1       |AUTO          |"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|CHEER_COMMENT|GREATER_THAN_OR_EQUAL|"1"          |
      |"피쳐드"|"피쳐드가 1개 이상이면 주는 뱃지"|"마켓용 설명입니다"     |1       |AUTO          |"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|"BADGE_IMG/738de23f/57b3cf32ed837/ic_like.png"|FEATURED_CREATOR|GREATER_THAN_OR_EQUAL|"1"          |
