@feature_artwork
Feature: Change artwork category

  Scenario: change category
    Given the admin "account1" logs in
    When the account "account1" changes the category of artworks in the file to "59849e3802586"
