@feature_partner_contract
Feature: Test on creator partner contract in admin site

  Scenario Outline: Create & update a partner contract
    Given the admin "account1" logs in
    And the creator "account2" logs in
    When the account "account1" creates a partner contract for the creator "account2" with <massUploader>
    Then the account "account1" validates a partner contract is created
    When the account "account1" gets partner contract list
    Then the account "account1" validates the created partner contract is in the partner contract list
    When the account "account1" updates the first partner contract in the list with creatorId <creatorId> and massUploader <updatedMassUploader>
    And the account "account1" gets partner contract list
    Then the account "account1" validates the first partner contract in the list has creatorId <creatorId> and massUploader <updatedMassUploader>
    And the account "account1" cleans up all partner contracts

    Examples:
      |massUploader|creatorId    |updatedMassUploader|
      |true        |5698e0996f1c3|false              |
      |false       |5698e0996f1c3|true               |

  Scenario: Delete all partner contracts
    Given the admin "account1" logs in
    When the account "account1" gets partner contract list
    And the account "account1" delete all partner contracts
    And the account "account1" gets partner contract list
    Then the account "account1" validates partner contract list is empty
